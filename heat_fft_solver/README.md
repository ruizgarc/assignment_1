# Heat Equation Solver using FFT

## Students

- Savyaraj Ravindra Deshmukh
- Alberto Ruiz García

## Description

Implementation of a Heat Equation Solver based on a particle code which uses a Fast Fourier Transform to solve the differential equation.

## Usage

To compile the code, run the following commands from the `build` directory `heat_fft_solver/build`
~~~bash
cmake .. && make
~~~

To launch the simulations, run the following command from the `bin` directory (`heat_fft_solver/build/bin/`) 

~~~bash
./particles [nsteps] [dump_freq] [input.csv] [particle_type] [timestep] 

            nsteps                  number of integration steps
            dump_freq               frequency of data dump
            input.csv               input file for initial temperature and heat rate field
            particle_type           can be: planet, ping_pong, material_point (we use material_point for this exercise)
            timestep                timestep for integration

~~~

The results are stored in csv format in the `dumps` directory (`heat_fft_solver/dumps/`)

Dirichlet boundary conditions are implemented and can be specified through the input file as boolean values, such that the temperature at these points will 
not be updated during the time evolution. 

## Data input and generation
Data input files are stored in the folder `data`, under `heat_fft_solver/data`.

To generate a new input file, you can use the python script `generate_input.py` as follows: 

~~~bash
usage: generate_input.py [-h] {planets,ping_pong,material} number filename {uniform,sine,linear,radial,test} {dirichlet,none} [heat_radius]

positional arguments:
  {planets,ping_pong,material}
                        Simulation type
  number                number of particles
  filename              name of generated input file
  {uniform,sine,linear,radial,test}
                        Heat source distrbution
  {dirichlet,none}      boundary condition on temperature field, options: 'dirichlet', 'none'
  heat_radius           Radius of the radial heat distribution

optional arguments:
  -h, --help            show this help message and exit
~~~

## Examples 
##### Launch simulation with radial heat source and Dirichlet boundary conditions (512 x 512 particles, R = 0.5)

1. Generate input file from `heat_fft_solver/data/`
```bash
    python3 generate_input.py material 512 data_radial_BC.csv radial dirichlet 0.5
```
2. Run simulation from `heat_fft_solver/build/bin/`
```bash
./particles 1000 100 data_radial_BC.csv material_point 0.1 
```
This launches a simulation for 1000 euler integration steps with timestep of 0.1 and the results are stored after every 100 iterations. Note the boundary condition of temperature to be zero at boundaries of the domain

##### Launch simulation with sinusouidal heat source and no boundary conditions (512 x 512 particles)

1. Generate input file from `heat_fft_solver/data/`
```bash
    python3 generate_input.py material 512 data_sine.csv sine none
```
2. Run simulation from `heat_fft_solver/build/bin/`
```bash
./particles 1000 100 data_sine.csv material_point 0.1 
```

##### Launch simulation with linear heat source and no boundary conditions (512 x 512 particles)

1. Generate input file from `heat_fft_solver/data/`
```bash
    python3 generate_input.py material 512 data_linear.csv linear none
```
2. Run simulation from `heat_fft_solver/build/bin/`
```bash
./particles 1000 100 data_sine.csv material_point 0.1 
```

##### Launch simulation with no heat source, uniform temperature, and no boundary conditions (512 x 512 particles)

1. Generate input file from `heat_fft_solver/data/`
```bash
    python3 generate_input.py material 512 data_uniform.csv uniform none
```
2. Run simulation from `heat_fft_solver/build/bin/`
```bash
./particles 1000 100 data_uniform.csv material_point 0.1 
```

## Dependencies

- FFTW3
- Googletest
- Doxygen (for documentation)

## Description of the particles organization (Exercise 1)
The main classes related to the organization of the particles are briefly explained below.

### Class: MaterialPoint
- This class is derived from the Particle class to represent grid points of a material. In addition to the Particle variables, it also contains the temperature and heat rate fields at the particle position. Additionally, a flag is added to fix the particle temperature during time evolution, so that (Dirichlet) boundary conditions can be easily integrated.

## Template: Matrix
- This template provides a container to hold an array of particles in a matrix form. It contains methods to access and manipulate the particles in the array and iterate through them in a more "human" way using tupled indices which improves the readability of the code. 

### Class: MaterialPointsFactory
- This class is used to create the simulation. It first creates a pointer to a unique instance of SystemEvolution. Then it reads the particles from file, assessing that the domain is square, and loads them into the system. Finally, it adds the temperature computation to the SystemEvolution instance and returns a pointer to it, which provides access from the main program. 

### Class: FFT
- Wrapper for the FFTW3 library (Fastest Fourier Transform in the West). Provides an easy interface between the created classes to hold the data (Matrix) and the internal classes from the FFTW3 library. This way the user can just apply Fourier transforms to Matrix objects by just looking at the function prototypes.
