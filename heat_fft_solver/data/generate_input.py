import numpy as np
import os
import argparse
import math

parser = argparse.ArgumentParser()
parser.add_argument("type", choices = ["planets", "ping_pong", "material"], help="Simulation type")
parser.add_argument("number", help="number of particles", type=int)
parser.add_argument("filename", help="name of generated input file")
parser.add_argument("heat_source", help="Heat source distrbution", choices =
        ["uniform", "sine", "linear", "radial","test"])
parser.add_argument("boundary_condition",nargs='?', default = "none",  help="boundary condition on temperature field, optiions: 'dirichlet', 'none'", choices=["dirichlet","none"])
parser.add_argument("heat_radius", nargs='?', help="Radius of the radial heat distribution", default = 0.3)

args = parser.parse_args()
if(args.type == "planets"):
    positions = np.random.random((args.number, 3))
    velocity = np.zeros((args.number, 3))
    force = velocity.copy()
    masses = 1e9*(np.random.random((args.number, 1)) + 1)
    names = []
    for idx in range(args.number):
        names[idx] = "planet_"+str(idx)

    file_data = np.hstack((positions, velocity, force, masses, names))

elif(args.type == "ping_pong"):
    positions = np.random.random((args.number, 3))
    velocity = np.zeros((args.number, 3))
    force = velocity.copy()
    masses = 1e9*(np.random.random((args.number, 1)) + 1)
    radii = args.radius * np.ones(args.number,1)

    file_data = np.hstack((positions, velocity, force, masses, radii))

elif (args.type == "material"):

    # Grid dimensions
    x_min = -1.
    x_max = 1.
    y_min = -1.
    y_max = 1.
    Lx = x_max - x_min
    Ly = y_max - y_min
    L = Lx
    dx = Lx/(args.number)
    dy = Ly/(args.number)

    n_material_points = args.number * args.number
    positions = np.zeros((n_material_points, 3))
    velocity = positions.copy()
    force = positions.copy()
    masses = np.zeros((n_material_points, 1))
    temperatures = masses.copy()
    heat_source = masses.copy()
    boundary_conditions = masses.copy()

    # Populate positions, temperatures, and heat rates
    for i in range(args.number):
        for j in range(args.number):
            idx_rowmajor = i + args.number * j
            x_particle = x_min + i * dx
            y_particle = y_min + j * dy
            z_particle = 0
            positions[idx_rowmajor] = [x_particle, y_particle, z_particle]

            if (args.heat_source =="uniform"):
                heat_source[idx_rowmajor] = 0
                temperatures[idx_rowmajor] = 20
            elif (args.heat_source == "sine"):
                heat_source[idx_rowmajor] = 4 * math.pi * math.pi/(L * L) * np.sin(2 * math.pi * x_particle/L)
                temperatures[idx_rowmajor] = np.sin(2 * math.pi * x_particle/L)
            elif (args.heat_source == "linear"):
                if (x_particle <= -0.5):
                    temperatures[idx_rowmajor] = -x_particle - 1
                elif (x_particle > -0.5 and x_particle <= 0.5):
                    temperatures[idx_rowmajor] = x_particle
                elif (x_particle > 0.5):
                    temperatures[idx_rowmajor] = -x_particle + 1

                if (abs(x_particle - 0.5) < dx):
                    heat_source[idx_rowmajor] = 1
                elif (abs(x_particle + 0.5) < dx):
                    heat_source[idx_rowmajor] = -1

            elif (args.heat_source == "radial"):
                heat_radius = float(args.heat_radius)
                if(x_particle**2 + y_particle**2 <
                        heat_radius**2):
                    heat_source[idx_rowmajor] = 1
                    temperatures[idx_rowmajor] = 0
                else:
                    heat_source[idx_rowmajor] = 0
                    temperatures[idx_rowmajor] = 0

            elif (args.heat_source == "test"):
                heat_source[idx_rowmajor] = 4 * math.pi * math.pi/(L*L) *  \
                        (x_particle * y_particle -
                                np.cos(2*math.pi*x_particle/L) + np.sin(2 * math.pi*y_particle/L));
                temperatures[idx_rowmajor] = (x_particle + y_particle +
                        x_particle**2 + y_particle**2)
            
            if args.boundary_condition == "dirichlet":
                if (i==0 or i==args.number-1 or j==0 or j==args.number-1):
                    boundary_conditions[idx_rowmajor] = int(1)
            else:
                boundary_conditions[idx_rowmajor] = int(0)

    file_data = np.hstack((positions, velocity, force, masses,
                temperatures, heat_source, boundary_conditions))

else:
    print("Error, not recognized! Type 'python3 generate_input.py -h' for help")
    sys.exit(1)

np.savetxt(args.filename, file_data, delimiter=" ")


def find_nearest(array,value):
    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or math.fabs(value - array[idx-1]) < math.fabs(value - array[idx])):
        return array[idx-1]
    else:
        return array[idx]
