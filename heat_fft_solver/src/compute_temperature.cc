#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

// Constructor 
ComputeTemperature::ComputeTemperature(Real dt) : dt(dt) {}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {

    UInt N = system.getNbParticles();
    UInt n_rows = sqrt(N);

    // Create matrix of current temperature field
    Matrix<complex> T(n_rows);
    Matrix<complex> h_v(n_rows);

    for(UInt p=0; p < N; p++){
        auto& par = static_cast<MaterialPoint&>(system.getParticle(p));
        T.storage[p] = par.getTemperature();
        h_v.storage[p] = par.getHeatRate();

    }

    // Compute the FFT frequencies
    Matrix<complex> freq_matrix = FFT::computeFrequencies(n_rows);

    // Fourier trasform the temperature 
    Matrix<complex> T_fft = FFT::transform(T);
    T_fft/=N; // Normalization factor

    Matrix<complex> hv_fft = FFT::transform(h_v);
    hv_fft/=N; // Normalization factor


    //std::cout << this->C_1 << " " << this->C_2 << std::endl; 
    // Compute temperature time derivative in Fourier domain
    Matrix<complex> dT_fft(n_rows);
    for (UInt i = 0; i < n_rows; i++){
        for (UInt j = 0; j < n_rows; j++){
                dT_fft(i,j) = this->C_1 * (hv_fft(i,j) - this->C_2 * T_fft(i,j) * norm(freq_matrix(i,j)));
                //std::cout << freq_matrix(i,j) << " " << freqs(i,j) << std::endl; 
        }
    }

    // Inverse fourier trasform of the time derivative
    Matrix<complex> dT = FFT::itransform(dT_fft);

    // Update temperatures using Euler integration step 
    for(UInt p=0; p < N; p++){
        auto& par = static_cast<MaterialPoint&>(system.getParticle(p));

        if (par.getBCFlag()){
            continue;
        }
        else{
            par.getTemperature() += this->dt * dT.storage[p].real();
        }
    }

    return;
}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::PrepareSimulation(System& system){

    UInt N = system.getNbParticles();
    UInt n_rows = sqrt(N); 

    // Loop through particles and get position
   Vector particle_pos; 
   //Real x_min, x_max, y_min, y_max;
   for(UInt p=0; p < N; p++){
        auto& par = static_cast<MaterialPoint&>(system.getParticle(p));
        particle_pos = par.getPosition();
        if (particle_pos[0] > x_max){
            x_max = particle_pos[0];
        } else if (particle_pos[0] < x_min){
            x_min = particle_pos[0]; 
        }

        if (particle_pos[1] > y_max){
            y_max = particle_pos[1];
        } else if (particle_pos[1] < y_min){
            y_min = particle_pos[1]; 
        }
    }

    // Grid dimensions
    Real L     = x_max - x_min;

    // Constants for heat equation (= f(grid, material))
    Real normalization_constant = 4 * M_PI * M_PI/(L * L); 

    this->C_1 = 1.0/(this->mass_density * this->specific_heat_capacity);
    this->C_2 = this->thermal_conductivity * normalization_constant; 

    return;

}

