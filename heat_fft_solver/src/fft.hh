#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
#include <iostream>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> applyTransform(Matrix<complex>& m_in, int flag); 
  static Matrix<complex> transform(Matrix<complex>& m_in);
  static Matrix<complex> itransform(Matrix<complex>& m_in);
  static Matrix<std::complex<Real>> computeFrequencies(UInt N);

};

/* -------------------------------------------------------------------------- */
/* Template implementation */
/* -------------------------------------------------------------------------- */
inline Matrix<complex> FFT::applyTransform(Matrix<complex>& m_in, int flag) {

  int N = (int)m_in.storage.size(); 
  Matrix<complex> m_out(m_in.size()); 
  fftw_complex *data_array; 
  fftw_plan plan;

  // Allocate input and output data arrays
  //data_array = (fftw_complex*)m_in.data();
  data_array = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N); 

  // Prepare plan 
  plan = fftw_plan_dft_2d(m_in.size(), m_in.size(),
   data_array, data_array,
   flag, FFTW_MEASURE); 

  // Test boundary conditions
  // plan = fftw_plan_r2r_2d(m_in.size(), m_in.size(),
  // data_array, data_array, FFTW_REDFT10, FFTW_RODFT10, FFTW_MEASURE);

  // Order the input array in row major ordering (C style)
  for (UInt i = 0; i < m_in.size(); i++){
    for (UInt j = 0; j < m_in.size(); j++) {
      data_array[i * m_in.size() + j][0] = m_in(i,j).real(); 
      data_array[i * m_in.size() + j][1] = m_in(i,j).imag(); 
      //std::cout << *data_array[i * m_in.size() + j] << std::endl;
    }
  }

  // Compute FFT
  fftw_execute(plan); 

  // Dump results into matrix class respecting order
  for (UInt i = 0; i < m_in.size(); i++){
    for (UInt j = 0; j < m_in.size(); j++) {
      m_out(i,j) = *data_array[i * m_in.size() + j]; 
    }
  }

  // Free memory
  fftw_destroy_plan(plan);
  fftw_free(data_array); 

  return m_out; 
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {

  int flag = FFTW_FORWARD;
  Matrix<complex> m_out = applyTransform(m_in, flag); 

  return m_out; 
  
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {

  int flag = FFTW_BACKWARD;
  Matrix<complex> m_out = applyTransform(m_in, flag); 
  //m_out/=m_in.storage.size();

  return m_out; 

}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::computeFrequencies(UInt N) {
/**
* Compute the angular frequencies for a square domain
* @param N : number of rows/columns of the matrix
*/

// Missing sample rate? Check slides and lecture
  Matrix<complex> freq_matrix(N);  // Matrix holding freqs

  // For imaginary unit
  using namespace std::complex_literals;

  complex freq; 
  UInt idx = 0; 
  for (UInt i = 0; i < N; i++){ 
      for(UInt j = 0; j < N; j++){
          freq = 0.0; 
          if(2*i<N){
              freq += ((double)i) /N;
          }else{
              freq += -((double)(N-i)) /N; 
          }

          if(2*j<N){
              freq+=(((double)j) / N)* 1i;
          }else{
              freq+=-(((double)(N-j)) / N) * 1i; ;
          }

          freq_matrix(i,j) = freq; 
          //std::cout << freq_matrix(i,j);
      }
      //std::cout << std::endl; 
  }

  return freq_matrix; 

}


/* ------------------------------------------------------ */


#endif