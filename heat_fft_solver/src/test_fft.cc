#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>
#include <fftw3.h>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <unistd.h>
#include "material_points_factory.hh"
#include "system.hh"

/*****************************************************************/
// Function prototypes
void AssertSimulationResults(UInt nsteps, UInt freq, std::string filename, Real timestep, Matrix<complex>& equilibrium_temperature); 
int GenerateInputFile(std::string outfile, std::string heat_source); 

/*****************************************************************/


void AssertSimulationResults(UInt nsteps, UInt freq, std::string filename, Real timestep, Matrix<complex>& equilibrium_temperature){

  // return an instance to the factory
  MaterialPointsFactory::getInstance();  

  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();

  SystemEvolution& evol = factory.createSimulation(filename, timestep);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);

  evol.evolve();  

    // Load results 
  std::string infile_path = get_current_dir_name();
  infile_path += "/../../dumps/step-00099.csv";

  Vector position, velocity, force;
  double temperature, heat_rate, mass; 
  std::ifstream infile(infile_path);
  std::string line;
 
  UInt idx = 0; 
  if (infile.is_open() == false) {
    std::cerr << "cannot open file " << infile_path << std::endl;
    throw;
  }

  while (infile.good()) {
    getline(infile, line);

    if (line[0] == '#' || line.size() == 0)
      continue;

    std::stringstream sstr(line);
    sstr >> position >> velocity >> force >> mass >> temperature >> heat_rate;

    ASSERT_NEAR(temperature, equilibrium_temperature.storage[idx].real(),1e-6); 
    idx++; 
  }
}

/*****************************************************************/


int GenerateInputFile(std::string outfile, std::string heat_source) {

  std::string delimiter = "/";
  std::string filename = "../../data/generate_input.py ";
  std::string arguments = "material 512 " + outfile + " " + heat_source + " none";
  std::string python_file = get_current_dir_name() + delimiter + filename + " "; 
  std::string command_execution = "python3 " + python_file + arguments; 

  int ret_command = system(command_execution.c_str()); 

  command_execution = "mv " + outfile + " ../../data";
  ret_command = system(command_execution.c_str());
  return ret_command; 

}


/*****************************************************************/

TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}

/*****************************************************************/

TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> m(N);
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  // Apply forward FFT and then apply inverse FFT
  Matrix<complex> res_fwd = FFT::transform(m); 


  Matrix<complex> res_bwd = FFT::itransform(res_fwd); 
  res_bwd/=(N*N); 

  // Compare the resutls
  for (UInt idx = 0; idx < m.storage.size(); idx++){
    ASSERT_NEAR(std::abs(m.storage[idx]), std::abs(res_bwd.storage[idx]), 1e-10);
  }


}

/*****************************************************************/


TEST(FFT, compute_frequencies) {

  // Number of samples
  UInt N = 512;
  Matrix<complex> m(N);
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  // Compute frequencies using inline function fft.hh
  Matrix<complex> freq_matrix = FFT::computeFrequencies(N);
 
  std::string filename = "../../src/fft_freqs.py ";
  std::string arguments = std::to_string(N); 
  std::string delimiter = "/"; 
  std::string outfile = "fft_freqs.out";
  std::string python_file = get_current_dir_name() + delimiter + filename + arguments; 
  std::string command_execution = "python3 " + python_file + " >> " + outfile;
  std::string command_removal = "rm ";
  command_removal += get_current_dir_name();
  command_removal += delimiter + outfile;

  // Execute commmand
  int ret_execution = system(command_execution.c_str()); 

  // Load results from python
  std::string infile_path = get_current_dir_name() + delimiter + outfile;
  std::ifstream myfile(infile_path);
  std::vector<double> python_freqs;
  for (double a; myfile >> a;) {
      python_freqs.push_back(a);
   }

   // Assert they are the same
  for (UInt idx = 0; idx < N; idx++){
    ASSERT_NEAR(python_freqs[idx++], freq_matrix(idx,0).real(), 1e-10); 
  }

  // // Clean-up
  int ret_removal = system(command_removal.c_str());
}


/*****************************************************************/


TEST(FFT, uniform_distribution) {
  UInt N = 512;
  Matrix<complex> equilibrium_temperature(N);
  Real x_min = -1; 
  Real x_max = 1; 
  Real y_min = -1; 
  Real y_max = 1;
  Real L = x_max - x_min; 
  Real dx = L/N; 
  Real x; 
  for (auto&& entry : index(equilibrium_temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = 20; 
  }

  // Setup up simulation
  UInt nsteps = 100; 
  UInt freq_dumps = 99;
  std::string filename = "data_uniform.csv";
  Real timestep = 0.1;

  // Generate input file 
  int ret_execution = GenerateInputFile(filename, "uniform"); 

  AssertSimulationResults(nsteps, freq_dumps, filename, timestep, equilibrium_temperature); 

  std::string cmd_removal = "rm ../../data/";
  cmd_removal += filename;
  int ret_removal = system(cmd_removal.c_str());
}



/*****************************************************************/


TEST(FFT, sinusoidal_distribution) {
  UInt N = 512;
  Matrix<complex> equilibrium_temperature(N);
  Real x_min = -1; 
  Real x_max = 1; 
  Real y_min = -1; 
  Real y_max = 1;
  Real L = x_max - x_min; 
  Real dx = L/N; 
  Real k = 2 * M_PI/L;
  Real x; 
  for (auto&& entry : index(equilibrium_temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    x = x_min + dx * i; 
    val = sin(k * x);
  }

  // Setup up simulation
  UInt nsteps = 100; 
  UInt freq_dumps = 99;
  std::string filename = "data_sine.csv";
  Real timestep = 0.1;

  // Generate input file 
  int ret_execution = GenerateInputFile(filename, "sine"); 

  AssertSimulationResults(nsteps, freq_dumps, filename, timestep, equilibrium_temperature); 

    std::string cmd_removal = "rm ../../data/";
  cmd_removal += filename;
  int ret_removal = system(cmd_removal.c_str());
}


/*****************************************************************/

TEST(FFT, linear_distribution) {
  UInt N = 512;
  Matrix<complex> equilibrium_temperature(N);
  Real x_min = -1; 
  Real x_max = 1; 
  Real y_min = -1; 
  Real y_max = 1;
  Real L = x_max - x_min; 
  Real dx = L/N; 
  Real x; 
  for (auto&& entry : index(equilibrium_temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    x = x_min + dx * i; 
    
    if (x <= -0.5){
      val = -x -1;
    } else if (x > -0.5 && x <= 0.5){
      val = x; 
    } else if (x>0.5){
      val = - x+1; 
    }
  }

  // Setup up simulation
  UInt nsteps = 100; 
  UInt freq_dumps = 99;
  std::string filename = "data_linear.csv";
  Real timestep = 0.1;

  // Generate input file 
  int ret_execution = GenerateInputFile(filename, "linear"); 

  AssertSimulationResults(nsteps, freq_dumps, filename, timestep, equilibrium_temperature); 

  std::string cmd_removal = "rm ../../data/";
  cmd_removal += filename;
  int ret_removal = system(cmd_removal.c_str());
}


/*****************************************************************/
