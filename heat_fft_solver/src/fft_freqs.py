import numpy as np
import os
import sys

def main():
    """
    Small program to test the function FFT::computeFrequencies

    """
    size = int(sys.argv[1])
    signal = np.zeros((size,size), dtype=float)

    k = 2 * np.pi/size
    for i in range(0,size):
        for j in range(0,size):
            signal[i][j] = np.cos(k * i)

    fourier = np.fft.fft2(signal)

    dx = 1;

    # Freq1D = np.fft.fftfreq(size, d=timestep)
    FreqCompRows = np.fft.fftfreq(fourier.shape[0],d=dx)
    FreqCompCols = np.fft.fftfreq(fourier.shape[1],d=dx)
    
    # Print 1D
    #for idx in range(size):
    #    print(str(Freq1D[idx])+" ", end = '')
    #print("\n----------------------------------------------------------------")

    # Print Rows    
    for idx in range(FreqCompRows.size):
        print(str(FreqCompRows[idx])+" ", end = '')
    #print("\n----------------------------------------------------------------")

    # Print Cols
    #for idx in range(FreqCompRows.size):
    #    print(str(FreqCompCols[idx])+" ", end = '')
    #print("\n----------------------------------------------------------------")


if __name__ == '__main__':
    main();
