#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "matrix.hh"

//! Compute temperature 
class ComputeTemperature : public Compute {

  // Virtual implementation
public:

  //! Temperature computation using FFT
  ComputeTemperature(Real dt);

  //! Override mother class
  void compute(System& system) override;

  //! Simulation preliminary calculations
  void PrepareSimulation(System& system); 

private:
Real dt; // Timestep
Real specific_heat_capacity = 1.0;
Real mass_density = 1.0;
Real thermal_conductivity = 1.0;
Real C_1, C_2; // Constants in heat equation ( = f(grid, material))
Real x_min, x_max, y_min, y_max; // Boundaries of the domain 

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
