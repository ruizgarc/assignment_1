# Import modules
import scipy.optimize
import numpy as np
import scipy.sparse.linalg
import matplotlib.pyplot as plt

def optimize(objective_function,
        optimization_method = "scipy",
        scipy_optimization_algorithm = "BFGS",
        plotting = False, *args, **kwargs):

    """
    Calls the selected optimizer and plots the results if enabled

    :param objective_function: Function to minimize
    :type objective_function: string
    :param optimization_method: Optimizer to use
    :type optimization_method: string
    :param scipy_optimization_algorithm: algorithm to use in scipy
    :type scipy_optimization_algorithm: string
    :param plotting: Enable/disable plotting
    :type plotting: boolean
    :param system_matrix: matrix A of the system Ax = b
    :type system_matrix: np.array
    :param output_vector: output vector b of the system Ax = b
    :type output_vector: np.array
    :param initial_guess: initial guess to start the optimization
    :type initial_guess: np.array
    """

    # System definition and initial guess
    A = kwargs['system_matrix']
    b = kwargs['output_vector']
    x_0 = kwargs['initial_guess']

    # Function to hold the intermediate optimizer iterations
    x_history_list = []
    x_history_list.append(x_0)
    def store_iterations(xk):
        x_history_list.append(xk)

    # Decide optimization method
    if (optimization_method == "scipy"):
        res = scipy.optimize.minimize(objective_function,
                x0 = x_0,
                method = scipy_optimization_algorithm,
                args= (A,b,),
                callback = store_iterations)
    elif (optimization_method == "gmres"):
        res =  scipy.sparse.linalg.lgmres(A,
                b,
                x0 = x_0,
                tol = 1e-9,
                callback = store_iterations)

    # Plot results
    if (plotting):
        x_contour, y_contour, f_contour = calculate_contours(x_0, A, b, n_pts_contours = 40)
        plot_iterations(x_history_list, x_contour, y_contour, f_contour, A, b,
                opt_method = optimization_method)

    # Return optimization result and number of iterations
    n_iterations = len(x_history_list) - 1
    return res, n_iterations

# ----------------------------------------------------------------------------

def quadratic_function(x, A, b):
    """
    Quadratic function to minimize in order to solve the linear sytem Ax = b

    :param x: state vector
    :type x: np.array
    :param A: system matrix
    :type A: np.array
    :param b: output vector
    :type b: np.array
    """

    S = 0.5 * np.dot(x.T, np.dot(A, x)) - np.dot(x.T, b)

    return S.ravel();

# ----------------------------------------------------------------------------

def calculate_contours(x_0, A, b, n_pts_contours = 50):
    """
    Computes the grids for the contours and the values of the quadratic
    function at these points

    :param x_0: initial guess
    :type x_0: np.array
    :param A: system matrix
    :type A: np.array
    :param b: output vector
    :type b: np.array
    :param n_pts_contours: number of points for each axis in the contours
    :type n_pts_contours: int
    :param components_to_plot: components to show in the contours
    :type components_to_plot: list
    """

    x1_max = 1.2 * np.abs(x_0[0])
    x2_max = 1.2 * np.abs(x_0[1])

    x1 = np.linspace(-x1_max, x1_max, n_pts_contours)
    x2 = np.linspace(-x2_max, x2_max, n_pts_contours)

    [x_contour, y_contour] = np.meshgrid(x1, x2)

    f_contour = np.zeros((n_pts_contours, n_pts_contours))
    for idx1 in range (0, len(x1)):
        for idx2 in range (0, len(x2)):
            xk = np.zeros((2,1))
            xk[0] = x_contour[idx1][idx2]
            xk[1] = y_contour[idx1][idx2]
            f_contour[idx1][idx2] = quadratic_function(xk, A, b)

    return x_contour, y_contour, f_contour

# ----------------------------------------------------------------------------

def plot_iterations(x_history_list, x_contour, y_contour, f_contour, A, b,
        *args, **kwargs):
    """
    Plot the contour values and the minimization path

    :param x_iterations: intermediate values during the optimization
    :type x_iterations: list
    :param x_contour: contour values for the first axis
    :type x_contour: np.array
    :param y_contour: contour values for the second axis
    :type y_contour: np.array
    :param z_contour: contour values for the vertical axis
    :type z_contour: np.array
    :param A: system matrix
    :type A: np.array
    :param b: output vector
    :type b: np.array
    :param opt_method: used optimization method to label the plot
    :type opt_method: string
    """

    # Calculate the objective function value from the stored values of x
    x_history = np.array(x_history_list)
    n_iterations = len(x_history_list)
    X_iter = np.zeros((n_iterations,3))
    for idx in range(0,n_iterations):
        xk = x_history[idx,:]
        X_iter[idx,0] = xk[0]
        X_iter[idx,1] = xk[1]
        X_iter[idx,2] = quadratic_function(xk, A, b)

    # Create 3D figure and plot iterations and contours
    fig = plt.figure()
    axe = fig.add_subplot(projection='3d')
    plt.plot(X_iter[:,0], X_iter[:,1], X_iter[:,2], '--or')
    plt.contour(x_contour, y_contour, f_contour)
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$")
    plt.title(kwargs['opt_method'])
    plt.show()

    return 0;

# ----------------------------------------------------------------------------

