# EXERCISE 1: SCIPY OPTIMIZATION
##### (Second exercise in branch `gmres`)



## Authors

- Alberto Ruiz García

- Savyaraj Deshmukh Ravindra

  

## Description

- The main program calls two different optimizers (BFGS and LGMRES) from `scipy` to solve a linear system `A·x = b`

- The path followed by the two optimizers towards the minimum is plotted for both optimization methods

  

## Contents

- **main.py**: call to the main program

- **optimizer.py**: calls to the different optimizers 

  

## Dependencies

- scipy

- numpy

- matplotlib

  

## Usage
```bash
usage: python3 main.py 
```



## Documentation

- Detailed documentation generated from docstrings is available in
  `docs/_build/html/index.html`.
