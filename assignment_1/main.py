# Python modules
import matplotlib.pyplot as plt
import numpy as np
import sys

# User-defined modules
import optimizer


# Main program
def main(*args, **kwargs):
    """
    Main program to call the different optimizers

    inputs: none
    """

    # Define system to miniize (A * x = b)
    A = np.array([[8,1], [1,3]])
    b = np.array([2,4])
    x0 = np.array([3,3])

    # Implement plotting using cli arguments
    display_plots = True

    # Run optimization using Scipy
    optimization_scipy, n_iter_scipy = optimizer.optimize(
            objective_function = optimizer.quadratic_function,
            optimization_method = "scipy",
            scipy_optimization_algorithm = "BFGS",
            system_matrix = A,
            output_vector = b,
            initial_guess = x0,
            plotting = display_plots)

    x_scipy = optimization_scipy.x
    f_scipy = optimization_scipy.fun

    # Run optimization using GMRES
    optimization_gmres, n_iter_gmres = optimizer.optimize(
            objective_function = optimizer.quadratic_function,
            optimization_method = "gmres",
            system_matrix = A,
            output_vector = b,
            initial_guess = x0,
            plotting = display_plots)

    x_gmres = optimization_gmres[0]
    f_gmres = optimizer.quadratic_function(x_gmres, A, b)

    # Display results
    print("SCIPY results: f_min = ",  f_scipy, ", x(f_min) = ", 
            x_scipy, "n_iter = ", n_iter_scipy)
    print("------------------------------------------------------------------")
    print("GMRES results: f_min = ",  f_gmres[0], ", x(f_min) = ", 
            x_gmres, "n_iter = ", n_iter_gmres)

    return 0

# Call to main
if __name__ == '__main__':
    main()
