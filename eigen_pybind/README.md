# Heat equation solver using finite differences + Eigen + Pybind

## Students
- Savyaraj Ravindra Deshmukh
- Alberto Ruiz García

## Description
Implementation of a Heat Equation Solver based on a particle code and a finite-difference scheme. Includes previous implementation of FFT. 

## Usage
To compile the code, run the following commands from the `build` directory `eigen_pybind/build`
~~~bash
cmake .. -DUSE_PYTHON=ON && make
~~~

To launch the simulations, run the following command from the `build` directory (`eigen_pybind/build`/) 

~~~bash
./particles nsteps dump_freq input.csv particle_type timestep method
	- nsteps: number of steps to simulate
	- dump_freq: frequency to dump results to .csv files
	- input.csv: path to the input file including format
	- particle_type: determines the type of simulation to run (options: planets, ping_pong, material_point)
	- timestep: to be used in the integrator
	- method: if particle_type == material_point we can choose between the FFT solver or the sparseLU solver using finite differences  (options: diff, fft). 
~~~

The results are stored in CSV format in the `dumps` directory (`eigen_pybind/dumps/`) 

Dirichlet boundary conditions are implemented and can be specified through the input file as boolean values, such that the temperature at these points will not be updated during the time evolution. 

## Data input and generation 

Data input files are stored in the folder `data`, under `eigen_pybind/data`.

To generate a new input file, you can use the python script `generate_input.py` as follows: 

~~~bash
usage: generate_input.py [-h] {planets,ping_pong,material} number filename {uniform,sine,linear,radial,test} {dirichlet,none} [heat_radius]

positional arguments:
  {planets,ping_pong,material}
                        Simulation type
  number                number of particles
  filename              name of generated input file
  {uniform,sine,linear,radial,test}
                        Heat source distrbution
  {dirichlet,none}      boundary condition on temperature field, options: 'dirichlet', 'none'
  heat_radius           Radius of the radial heat distribution

optional arguments:
  -h, --help            show this help message and exit
~~~

## Comments on pybind

1. The **createSimulation** function has been overloaded to take functor as an argument. This can be used to quickly edit the system properties or implement new functionalities if needed. So we gain plenty of flexibility in terms of novel features
2. To properly manage references to **Compute** objects, shared pointers are used as object holders
## Tests

An additional test to the previous assignments have been added to assess the validity of the sparseLU solver using finite differences. It is defined in `test_diff.cc` and its executable called **test_diff**. It includes the same tests as those defined for the FFT solver, checking the equilibrium temperature of the heated square plate after a number of iterations and comparing with the analytical solution. 

## Examples:

### Running the C++ code from Python using Pybind

##### Launch simulation for 10 planets

1. Generate input file from `eigen_pybind/data/`
```bash
    python3 generate_input.py planets 10 planets.csv
```
1. Run simulation from `eigen_pybind/build/` 
```bash
python3 main.py 200 10 ../data/planets.csv planet 0.1
```
This launches a simulation for 200 steps with timestep of 0.1 and the results are stored after every 10 iterations. 

##### Launch simulation for material particles with radial heat source and Dirichlet boundary conditions (512 x 512 particles, R = 0.5)

1. Generate input file from `eigen_pybind/data/`
```bash
    python3 generate_input.py material 512 data_radial_BC.csv radial dirichlet 0.5
```
1. Run simulation from `eigen_pybind/build/` 
```bash
python3 main.py 1000 100 ../data/data_radial_BC.csv material_point 0.1
```
This launches a simulation for 1000 euler integration steps with timestep of 0.1 and the results are stored after every 100 iterations. Note the boundary condition of temperature to be zero at boundaries of the domain

### Running the C++ code (standalone) using finite differences and the Eigen library

##### Launch simulation with radial heat source and Dirichlet boundary conditions (512 x 512 particles, R = 0.5)

1. Generate input file from `eigen_pybind/data/`
```bash
    python3 generate_input.py material 512 data_radial_BC.csv radial dirichlet 0.5
```
2. Run simulation from `eigen_pybind/build/` using the finite difference solver
```bash
./particles 1000 100 ../data/data_radial_BC.csv material_point 0.1 diff
```
This launches a simulation for 1000 euler integration steps with timestep of 0.1 and the results are stored after every 100 iterations. Note the boundary condition of temperature to be zero at boundaries of the domain

##### Launch simulation with sinusouidal heat source and no boundary conditions (512 x 512 particles)

1. Generate input file from `eigen_pybind/data/`
```bash
    python3 generate_input.py material 512 data_sine.csv sine none
```
2. Run simulation from `eigen_pybind/build/` using the finite differences solver
```bash
./particles 1000 100 ../data/data_sine.csv material_point 0.1 diff
```

##### Launch simulation with linear heat source and no boundary conditions (512 x 512 particles)

1. Generate input file from `eigen_pybind/data/`
```bash
    python3 generate_input.py material 512 data_linear.csv linear none
```
2. Run simulation from `eigen_pybind/build/` using the finite difference solver
```bash
./particles 1000 100 ../data/data_sine.csv material_point 0.1 diff
```

##### Launch simulation with no heat source, uniform temperature, and no boundary conditions (512 x 512 particles)

1. Generate input file from `eigen_pybind/data/`
```bash
    python3 generate_input.py material 512 data_uniform.csv uniform none
```
2. Run simulation from `eigen_pybind/build/` using the finite difference solver
```bash
./particles 1000 100 ../data/data_uniform.csv material_point 0.1 diff
```



## Dependencies

- Eigen
- Pybind
- Googletest
- Doxygen (for documentation)
