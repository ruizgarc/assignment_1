#include "my_types.hh"
#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <unistd.h>
#include "material_points_factory_diff.hh"
#include "system.hh"

/*****************************************************************/
// Function prototypes
void AssertSimulationResults(UInt N_particles, UInt nsteps, UInt freq, std::string filename, Real timestep,Eigen::MatrixXd& equilibrium_temperature); 
int GenerateInputFile(std::string outfile, std::string heat_source, UInt N_particles); 

/*****************************************************************/

void AssertSimulationResults(UInt N_particles, UInt nsteps, UInt freq, std::string filename, Real timestep, Eigen::MatrixXd& equilibrium_temperature){

  // return an instance to the factory
  MaterialPointsFactoryDiff::getInstance();  

  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();

  std::string datapath = "../data/"+filename;
  SystemEvolution& evol = factory.createSimulation(datapath, timestep);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);

  evol.evolve();  

    // Load results 
  std::string infile_path = get_current_dir_name();
  infile_path += "/../dumps/step-00019.csv";

  Vector position, velocity, force;
  double temperature, temperature_rate, heat_rate, mass, init_temp;  
  std::ifstream infile(infile_path);
  std::string line;

  std::ifstream init_file(datapath);
  if (init_file.is_open() == false) {
    std::cerr << "cannot open file " << datapath << std::endl;
    throw;
  }

  if (infile.is_open() == false) {
    std::cerr << "cannot open file " << infile_path << std::endl;
    throw;
  }

  UInt idx = 0; 
  while (infile.good()) {
    getline(infile, line); // Load simulation results

    if (line[0] == '#' || line.size() == 0)
      continue;

    std::stringstream sstr(line);
    sstr >> position >> velocity >> force >> mass >> temperature >> temperature_rate >> heat_rate;

    UInt row = (UInt)(idx/N_particles);
    UInt column = idx - row * N_particles; 

   // Load initial temperatures
   if (init_file.good()) {
    getline(init_file, line);
    if (line[0] == '#' || line.size() == 0)
      continue;
    std::stringstream sstr(line);
    sstr >> position >> velocity >> force >> mass >> init_temp >> temperature_rate >> heat_rate;
  }

    ASSERT_NEAR(temperature, init_temp, 1e-4); 
    idx++; 
  }
}

/*****************************************************************/


int GenerateInputFile(std::string outfile, std::string heat_source, UInt N_particles) {

  std::string delimiter = "/";
  std::string filename = "../data/generate_input.py ";
  std::string arguments = "material " + std::to_string(N_particles) + " " + outfile + " " + heat_source + " none";
  std::string python_file = get_current_dir_name() + delimiter + filename + " "; 
  std::string command_execution = "python3 " + python_file + arguments; 

  int ret_command = system(command_execution.c_str()); 

  command_execution = "mv " + outfile + " ../data";
  ret_command = system(command_execution.c_str());
  return ret_command; 

}

/*****************************************************************/

TEST(HEAT_DIFF, uniform_distribution) {
  UInt N_particles = 120;
  Eigen::MatrixXd equilibrium_temperature(N_particles,N_particles);
  Real x_min = -1; 
  Real x_max = 1; 
  Real y_min = -1; 
  Real y_max = 1;
  Real L = x_max - x_min; 
  Real dx = L/(N_particles-1); 
  Real x; 

  for (UInt i = 0; i < N_particles; i++){
    for(UInt j = 0; j < N_particles; j++) {
      equilibrium_temperature(i,j) = 20;
    }
  }

  // Setup up simulation
  UInt nsteps = 20; 
  UInt freq_dumps = 1;
  std::string filename = "data_uniform.csv";
  Real timestep = 0.001;


  // Generate input file 
  int ret_execution = GenerateInputFile(filename, "uniform", N_particles); 

  AssertSimulationResults(N_particles, nsteps, freq_dumps, filename, timestep, equilibrium_temperature); 


}

/*****************************************************************/


TEST(HEAT_DIFF, sinusoidal_distribution) {
  UInt N_particles = 150;
  Eigen::MatrixXd equilibrium_temperature(N_particles,N_particles);
  Real x_min = -1; 
  Real x_max = 1; 
  Real y_min = -1; 
  Real y_max = 1;
  Real L = x_max - x_min; 
  Real dx = L/(N_particles-1); 
  Real k = 2 * M_PI/L;
  Real x; 
  for (UInt j = 0; j < N_particles; j++){
    for(UInt i = 0; i < N_particles; i++) {
      x = x_min + dx * i; 
      equilibrium_temperature(i,j) = sin(k * x);
    }
  }

  // Setup up simulation
  UInt nsteps = 20; 
  UInt freq_dumps = 1;
  std::string filename = "data_sine.csv";
  Real timestep = 0.001;


  // Generate input file 
  int ret_execution = GenerateInputFile(filename, "sine", N_particles); 

  AssertSimulationResults(N_particles, nsteps, freq_dumps, filename, timestep, equilibrium_temperature); 

}



/*****************************************************************/


TEST(HEAT_DIFF, linear_distribution) {
  UInt N_particles = 121;
  Eigen::MatrixXd equilibrium_temperature(N_particles,N_particles);
  Real x_min = -1; 
  Real x_max = 1; 
  Real y_min = -1; 
  Real y_max = 1;
  Real L = x_max - x_min; 
  Real dx = L/(N_particles-1); 
  Real x; 
  for (UInt i = 0; i < N_particles; i++){
      x = x_min + dx * i; 
    for(UInt j = 0; j < N_particles; j++) {
    if (x <= -0.5){
      equilibrium_temperature(i,j) = -x-1;
    } else if (x>0.5){
      equilibrium_temperature(i,j) = - x+1; 
    } else {
      equilibrium_temperature(i,j) = x; 
    }


  }
}

  // Setup up simulation
  UInt nsteps = 20; 
  UInt freq_dumps = 1;
  std::string filename = "data_linear.csv";
  Real timestep = 0.001;

  // Generate input file 
  int ret_execution = GenerateInputFile(filename, "linear", N_particles); 

  AssertSimulationResults(N_particles, nsteps, freq_dumps, filename, timestep, equilibrium_temperature); 

}

