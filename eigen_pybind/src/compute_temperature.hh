#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute heat equation evolution
class ComputeTemperature : public Compute {

public:
  //! temperature evolution implementation
  void compute(System& system) override;

  //! heat conductivity
  Real& getConductivity() { return conductivity; };
  void setConductivity(const Real coductivity) { this-> conductivity = conductivity;};
  //!  heat capacity
  Real& getCapacity() { return capacity; };
  void setCapacity(const Real capacity) { this-> capacity = capacity;};
  //!  heat capacity
  Real& getDensity() { return density; };
  void setDensity(const Real density) { this-> density = density;};
  //! characteristic length of the square
  Real& getL() { return L; };
  void setL(const Real L) { this-> L = L;};
  //! characteristic length of the square
  Real& getDeltat() { return delta_t; };
  void setDeltat(const Real delta_t) { this-> delta_t = delta_t;};

  bool implicit = true;

private:
  Real conductivity;
  Real capacity;
  Real density;
  //! side length of the problem
  Real L;

  Real delta_t;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
