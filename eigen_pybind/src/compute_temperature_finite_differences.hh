#ifndef COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
#define COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH

// Comment/Uncomment to disable/enable debug modes
//#define DEBUG_ENABLE 1
//#define DEBUG_MATRIX 1

#include "compute.hh"
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>

class ComputeTemperatureFiniteDifferences : public Compute {
public:
  ComputeTemperatureFiniteDifferences() {}

  void compute(System& system) override;
  void assembleLinearOperator(System& system);
  void assembleRightHandSide(System& system);


    //! return the heat conductivity
  Real& getConductivity() { return conductivity; };
  //! return the heat capacity
  Real& getCapacity() { return capacity; };
  //! return the heat capacity
  Real& getDensity() { return density; };
  //! return the characteristic length of the square
  Real& getL() { return L; };
  //! return the sampling time
  Real& getDeltat() { return delta_t; };

  //! Simulation preliminary calculations
  void prepareSimulation(System& system); 

  private:
  Real conductivity;
  Real capacity;
  Real density;
  //! side length of the problem
  Real L;

  Real delta_t;
  Real delta_x; 

  Eigen::VectorXd RHS;
  Eigen::SparseMatrix<Real> system_matrix; 
  Eigen::SparseLU<Eigen::SparseMatrix<Real>> solver;

  Real alpha, alpha_dx2; // Constants in heat equation ( = f(grid, material))
  Real x_min, x_max, y_min, y_max; // Boundaries of the domain 
};

#endif  // COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
