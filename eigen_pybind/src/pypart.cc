#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

namespace py = pybind11;

#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "particles_factory_interface.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "compute_interaction.hh"
#include "compute.hh"
#include "system.hh"
#include "system_evolution.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  py::class_<ParticlesFactoryInterface>(
    m, "ParticlesFactoryInterface")
  .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
  m, "MaterialPointsFactory")
  .def_property_readonly("system_evolution", &MaterialPointsFactory::getSystemEvolution, py::return_value_policy::reference)
  .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference)
  .def("createSimulation", py::overload_cast<const std::string&,
  Real, std::function<void(MaterialPointsFactory&, Real)>& >(&MaterialPointsFactory::createSimulation), py::return_value_policy::reference);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(
  m, "PlanetsFactory")
  .def_property_readonly("system_evolution", &PlanetsFactory::getSystemEvolution, py::return_value_policy::reference)
  .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference)
  .def("createSimulation", py::overload_cast<const std::string&,
  Real, std::function<void(PlanetsFactory&, Real)>& >(&PlanetsFactory::createSimulation), py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
  m, "PingPongBallsFactory")
  .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);//, py::return_value_policy::reference)

  py::class_<System>(
  m, "System")
  .def(py::init<>());

  py::class_<SystemEvolution>(
  m, "SystemEvolution")
  .def("setNSteps", &SystemEvolution::setNSteps)
  .def("setDumpFreq", &SystemEvolution::setDumpFreq)
  .def("evolve", &SystemEvolution::evolve)
  .def("getSystem", &SystemEvolution::getSystem)
  .def("addCompute", &SystemEvolution::addCompute);

  py::class_<Compute, std::shared_ptr<Compute>>(
  m, "Compute");

  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(
  m, "ComputeTemperature")
  .def(py::init<>())
  .def_property("conductivity", &ComputeTemperature::getConductivity, &ComputeTemperature::setConductivity)
  .def_property("capacity", &ComputeTemperature::getCapacity, &ComputeTemperature::setCapacity)
  .def_property("density", &ComputeTemperature::getDensity, &ComputeTemperature::setDensity)
  .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::setL)
  .def_property("deltat", &ComputeTemperature::getDeltat, &ComputeTemperature::setDeltat);

  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(
  m, "ComputeInteraction");

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(
  m, "ComputeGravity")
  .def(py::init<>()) 
  .def("setG", &ComputeGravity::setG);

  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(
  m, "ComputeVerletIntegration")
  .def(py::init<Real>())
  .def("addInteraction", &ComputeVerletIntegration::addInteraction);


  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(
  m, "CsvWriter")
  .def(py::init<const std::string&>())
  .def("write", &CsvWriter::write);
}
