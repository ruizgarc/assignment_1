#include "material_points_factory_diff.hh"
#include "compute_temperature_finite_differences.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include <cmath>
#include <iostream>
/* -------------------------------------------------------------------------- */

std::unique_ptr<Particle> MaterialPointsFactoryDiff::createParticle() {
  return std::make_unique<MaterialPoint>();
}

/* -------------------------------------------------------------------------- */

SystemEvolution&
MaterialPointsFactoryDiff::createSimulation(const std::string& fname,
                                        Real timestep) {

  this->system_evolution =
      std::make_unique<SystemEvolution>(std::make_unique<System>());

  CsvReader reader(fname);
  reader.read(this->system_evolution->getSystem())
;
  // check if it is a square number
  auto N = this->system_evolution->getSystem().getNbParticles();
  int side = std::sqrt(N);
  if (side * side != N)
    throw std::runtime_error("number of particles is not square");

  auto compute_temp = std::make_shared<ComputeTemperatureFiniteDifferences>();

  compute_temp->getConductivity() = 1.;
  compute_temp->getCapacity() = 1.;
  compute_temp->getDensity() = 1;
  compute_temp->getDeltat() = timestep;
  compute_temp->prepareSimulation(this->system_evolution->getSystem()); 
  compute_temp->assembleLinearOperator(this->system_evolution->getSystem()); 
  this->system_evolution->addCompute(compute_temp);
  
  return *system_evolution;
}

/* -------------------------------------------------------------------------- */

ParticlesFactoryInterface& MaterialPointsFactoryDiff::getInstance() {
  if (not ParticlesFactoryInterface::factory)
    ParticlesFactoryInterface::factory = new MaterialPointsFactoryDiff;

  return *factory;
}

/* -------------------------------------------------------------------------- */
