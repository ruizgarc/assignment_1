#ifndef __PLANETS_FACTORY__HH__
#define __PLANETS_FACTORY__HH__

/* -------------------------------------------------------------------------- */
#include "particles_factory_interface.hh"
#include "planet.hh"
/* -------------------------------------------------------------------------- */

//! Factory for planet simulations
class PlanetsFactory : public ParticlesFactoryInterface {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
private:
  PlanetsFactory() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:
  SystemEvolution& createSimulation(const std::string& fname,
                                    Real timestep) override;

  SystemEvolution& createSimulation(const std::string& fname,
  Real timestep, std::function<void(PlanetsFactory&, Real)> &createComputes);

  std::unique_ptr<Particle> createParticle() override;

  static ParticlesFactoryInterface& getInstance();

  SystemEvolution& getSystemEvolution() {return *system_evolution;};
};

/* -------------------------------------------------------------------------- */
#endif  //__PLANETS_FACTORY__HH__
