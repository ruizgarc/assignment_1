#include "compute_temperature_finite_differences.hh"
#include "material_point.hh"
#include "matrix.hh"
//#include "matrix_eigen_addons.hh"

void ComputeTemperatureFiniteDifferences::compute(System& system) {

  ComputeTemperatureFiniteDifferences::assembleRightHandSide(system); 

  Eigen::VectorXd temperatures = this->solver.solve(this->RHS); 

  // Assemble system matrix
  for(UInt par_idx=0; par_idx < system.getNbParticles(); par_idx++){
      auto& par = static_cast<MaterialPoint&>(system.getParticle(par_idx));
      par.getTemperature() = temperatures[par_idx];
    }

  return;

}


void ComputeTemperatureFiniteDifferences::assembleLinearOperator(
    System& system) {

  UInt N_particles = system.getNbParticles();
  UInt N = sqrt(N_particles); 

  this->system_matrix.resize(N_particles, N_particles); 
  this->RHS.resize(N_particles);

  // Assemble system matrix
  for(UInt par_idx=0; par_idx < N_particles; par_idx++){
      auto& par = static_cast<MaterialPoint&>(system.getParticle(par_idx));
      auto pos = par.getPosition(); 

          // Check limits
          if ((pos[0] > this->x_min) && (pos[0] < this->x_max) 
            && (pos[1] > this->y_min) && (pos[1] <this->y_max)){ // Interior point
            system_matrix.insert(par_idx,par_idx) = 1 + 4 * this->alpha_dx2; 
            system_matrix.insert(par_idx,par_idx - 1) = -this->alpha_dx2;
            system_matrix.insert(par_idx,par_idx + 1) = -this->alpha_dx2; 
            system_matrix.insert(par_idx,par_idx - N) = -this->alpha_dx2; 
            system_matrix.insert(par_idx,par_idx + N) = -this->alpha_dx2;
          } else { // Fix temperature (Dirichlet BC)
            system_matrix.insert(par_idx, par_idx) = 1; 
          }
   }

  // Compress for SparseLU
  system_matrix.makeCompressed(); 

  // Compute the ordering permutation vector from the structural pattern of the system matrix
  this->solver.analyzePattern(system_matrix); 
  //  //Compute the numerical factorization 
  this->solver.factorize(system_matrix); 

#ifdef DEBUG_MATRIX
  for (UInt j = 0; j < N_particles; j++){
    for(UInt i = 0; i < N_particles; i++){
      std::cout << system_matrix.coeffRef(i,j) << " ";
    }
    std::cout << std::endl; 
  }
#endif

  return;

}


void ComputeTemperatureFiniteDifferences::assembleRightHandSide(
    System& system) {

  UInt N_particles = system.getNbParticles();

  Real temperature;
  Real heat_source;
  Vector position; 
  for(UInt p=0; p < N_particles; p++){
      auto& par = static_cast<MaterialPoint&>(system.getParticle(p));
      temperature = par.getTemperature(); 
      heat_source = par.getHeatSource(); 
      position = par.getPosition();

    if ((position[0] > this->x_min) && (position[0] < this->x_max) 
            && (position[1] > this->y_min) && (position[1] <this->y_max)){ // Interior point
        this->RHS[p] = this-> alpha * heat_source + temperature;
      } else{ // Fix temperature (dirichlet BC)
        this->RHS[p] = temperature; 
      }
  }

  return;
}

void ComputeTemperatureFiniteDifferences::prepareSimulation(System& system){

    UInt N_particles = system.getNbParticles();
    UInt N = sqrt(N_particles); 

    // Loop through particles and get position
   Vector particle_pos; 
   this->x_max = 0; 
   this->x_min = 0; 
   this->y_max = 0; 
   this->y_min = 0; 
   for(UInt p=0; p < N_particles; p++){
        auto& par = static_cast<MaterialPoint&>(system.getParticle(p));
        particle_pos = par.getPosition();
        
        if (particle_pos[0] > this->x_max){
            this->x_max = particle_pos[0];
      } else if (particle_pos[0] < this->x_min){
            x_min = particle_pos[0]; 
        }

        if (particle_pos[1] > this->y_max){
            this->y_max = particle_pos[1];
        } else if (particle_pos[1] < this->y_min){
            this->y_min = particle_pos[1]; 
        }
    }

    // Grid dimensions
    this->L       = x_max - x_min;
    this->delta_x = L/(N-1); 

    // Constants for heat equation (= f(grid, material))
    this->alpha = this->delta_t/(this->density * this->capacity);
    this->alpha_dx2 = this->alpha/(this->delta_x * this->delta_x); 

// Output to screen
#ifdef DEBUG_ENABLE
    std::cout << "L          = " << this->L << std::endl; 
    std::cout << "delta_x    = " << this->delta_x << std::endl; 
    std::cout << "Nrows      = " << N << std::endl; 
    std::cout << "N_elem     = " << N_particles << std::endl; 
    std::cout << "alpha      = " << this->alpha << std::endl; 
    std::cout << "alpha/dx^2 = " << this->alpha_dx2 << std::endl; 
    std::cout << "x_min      = " << this->x_min << std::endl; 
    std::cout << "x_max      = " << this->x_max << std::endl; 
    std::cout << "y_min      = " << this->y_min << std::endl; 
    std::cout << "y_max      = " << this->y_max << std::endl; 
#endif

    return;

}



