### Assignments for Scientific Programming for Engineers

- Savyaraj Deshmukh Ravindra
- Alberto Ruiz García

### Homework \#1: `assignment_1`
### Homework \#2: `homework2`
### Assignment \#3: `heat_fft_solver`


### Note

We had to add the second assignment as a submodule and reorder the repository as we did not know that we were supposed to keep the same repository for all assignments. We have done that instead of copying the files directly so that you can check the git history. 

Best regards, 
Alberto and Savyaraj